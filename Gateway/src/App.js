import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Person from "./Person/Person";
class App extends Component {



  state =  {
   response:'', 
   persons : [
     {name : "Adil" ,age : "21"},
     {name : "Hassan" ,age : "20"},
     {name : "Usama" ,age : "22"}
            
   ],
   showpersons:false

   
  };
 /* switchNameHandler = (newName) => {

    this.setState({  
    persons: [  
      {name : newName ,age : "21"},
      {name : "Toti" ,age : "20"},
      {name : "Chooocha" ,age : "22"}
    
    ], 

  }
);
  */
//calling the callApi method set the state to api response.
  componentDidMount(){
    this.callApi()
    .then(res=>this.setState({response : res.express}))
    .catch(err=>console.log(err));
  }
//interacting with express
  callApi = async () => {
    const response = await fetch('/api/hello');
    const body = await response.json();

    if(response.status !== 200) throw Error (body.message);
    return body;
  }; 


  nameChangedHandler = (event) => 
  {
    this.setState({  
    persons: [  
      {name : event.target.value ,age : "21"},
      {name : "Toti" ,age : "20"},
      {name : "Chooocha" ,age : "22"}
    ]
  }
  );
  }  

  showNameHandler = () => {
    const show =this.state.showpersons;
    this.setState({showpersons: !show});
  }

  deletePersonHandler = (personIndex)=>{
    const persons = this.state.persons;
    persons.splice(personIndex,1);
    this.setState({persons: persons});
  }
    
  render() {

    const style = {
      backgroundColor:"green",
      color:"white",
      border: '1px solid',
      cursor : 'pointer',
      font: 'inherit',
      padding : '8px'



    };


    let persons = null;



    if(this.state.showpersons){
      persons= 
      (
      <div>
        {this.state.persons.map((person,index) => {
          return <Person click = {() => this.deletePersonHandler(index)} name = {person.name} age = {person.age}/>
        } )}
      {/*  <Person 
              name = {this.state.persons[0].name} 
              age ={this.state.persons[0].age}
              changed={this.nameChangedHandler}
        />
        <Person 
            name = {this.state.persons[1].name} 
            age ={this.state.persons[1].age}
            click= {this.switchNameHandler}
             />
        
        <Person name = {this.state.persons[2].name} age ={this.state.persons[2].age}/>
      */}

      </div>);
      style.backgroundColor="red";
    }
    const classes = [];
    if(this.state.persons.length <= 1){
      classes.push('red');

    }
    if(this.state.persons.length <=2){
      classes.push('bold');
    }




    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title" >Welcome to React</h1>
          <p className={classes.join(' ')}>Yes It Is Working </p>
        </header>
        <button 
        style ={style}
        /*onClick={this.switchNameHandler.bind(this,'Adil')}>*/>Swicth names </button>
        <button  style ={style} onClick={this.showNameHandler} >Show Names</button>
        {persons}
        <button style={style} onClick={this.componentDidMount}>GEt Response
        {this.state.response}
        </button>
        <p className="App-intro" onClick = {this.componentDidMount}>
        {this.state.response}
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
    </div>
    );
  }
}

export default App;
