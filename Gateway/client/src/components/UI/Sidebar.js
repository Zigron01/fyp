import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import React ,{ Component } from 'react';
import axios from 'axios';
import "./css/bootstrap.min.css";
import "./css/Navigation-with-Button.css";
import "./css/Sidebar-Menu.css";
import "./css/Sidebar-Menu.css";
import "./css/styles.css";
import { Container, Row, Col } from 'react-grid-system' ;
import {Tab} from 'react-bootstrap';
import Paper from 'material-ui/Paper';
import Forms2 from './Forms2';

class Sidebars extends  Component {
  constructor(props){
    super(props);
    this.state={
      Gateways:[],
      Mac:[],
      Form:true
    }

  }



componentDidMount(){
  this.setState({
    Form:false
  })
    console.log("get working")
      
    axios.get('/gateways')
      .then((response)=>{
        this.setState({
          Gateways:response.data.resp,
        })
        console.log(response.data.resp);
      })
    }

handleClickthree(mac){
  this.setState({
    Form:true
  })
      console.log(mac);
  
      axios.get(`/mac/${mac}`)
      .then((response)=>{
       this.setState({Mac:response.data})
        console.log(response)
        console.log(this.state.Mac);
      })
  
    } 

render()

{
  const style={
      
  }
  
  return(
    <div class="page-content-wrapper">
<div>  
  <div>
    <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
      <div class="container">
        <a class="navbar-brand" href="/">Gateway</a>
      <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
      <div class="collapse navbar-collapse" id="navcol-1">
        <ul class="nav navbar-nav mr-auto">
          <li class="nav-item" role="presentation"><a class="nav-link" href="/xmpp">Xmpp Settings</a></li>
          <li class="nav-item" role="presentation"><a class="nav-link" href="/con">Add Device</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Dropdown </a>
          <div class="dropdown-menu" role="menu">
            <a class="dropdown-item" role="presentation" href="#">First Item</a>
            <a class="dropdown-item" role="presentation" href="#">Second Item</a>
            <a class="dropdown-item" role="presentation" href="#">Third Item</a>
          </div>
      </li>
        </ul><span class="navbar-text actions">
        <a href="#" class="login">Log In</a>
        <a class="btn btn-light action-button" role="button" href="#">Sign Up</a></span>
      </div>
    </div>
</nav>
</div>
<div id="wrapper">
  <div id="sidebar-wrapper">
    {this.state.Gateways.map((dynamicdata,key) =>
  <ul class="sidebar-nav">
      <li> <a href="#" onClick={this.handleClickthree.bind(this,this.state.Gateways[0].Mac)}>{this.state.Gateways[0].Mac}</a></li>
      <li> <a href="#" onClick={this.handleClickthree.bind(this,this.state.Gateways[1].Mac)}>{this.state.Gateways[1].Mac}</a></li>
      <li> <a href="#" onClick={this.handleClickthree.bind(this,this.state.Gateways[2].Mac)}>{this.state.Gateways[2].Mac}</a></li>
  </ul>      
        )
    }
    
  
      
  </div>
</div>
</div>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/Sidebar-Menu.js"></script>

 
   
 <Row>
   <Col>
  </Col>
   <Col>
   </Col>
   <Col>
   {this.state.Form?
   <Forms2 Mac={this.state.Mac}/>
   :null
  }      
  </Col>
 </Row>
</div>
    
    

   

  );
}

}

export default Sidebars;