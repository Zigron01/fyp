import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import React ,{ Component,ReactPropTypes } from 'react';
import axios from 'axios';
import "./css/bootstrap.min.css";
import "./css/Navigation-with-Button.css";
import "./css/Sidebar-Menu.css";
import "./css/Sidebar-Menu.css";
import "./css/styles.css";
import { Container, Row, Col } from 'react-grid-system' ;
import {Tabs,Tab,MenuItem,DropdownItem,DropdownToggle,DropdownMenu,NavDropdown, Dropdown} from 'reactstrap';
import Paper from 'material-ui/Paper';
import Forms2 from './Forms2';
import $ from 'jquery'
import spinner from './spinner';
import Submenu from './Submenu';
import CSSTransitionGroup from 'react-addons-css-transition-group'


class Sidebars extends  Component {
  constructor(props){
     
  
    super(props);
    this.state={
      Gateways:[],
      Mac:[],
      dropdownOpen: false,
      dynamicdata:[],
      showAboutMenu:true,
      Form:true
    }

  
  }



  
componentDidMount(){
  
  this.setState({ 
    showAboutMenu: false 
  });
  
  this.setState({
    Form:false
  })
  console.log("get working")
  axios.get('/gateways')
      .then((response)=>{
        this.setState({
          Gateways:response.data.resp,
        })
        console.log(response.data.resp);
      })
}
handleClick(){
  
  axios.get('/get_dev')
  .then((response)=>{
    this.setState({
      Gateways:response.data.resp,
    })
    console.log(response.data.resp);
  })

}
handleClickthree(mac){
  this.setState({
    Form:true
  })
  console.log(mac);
  axios.get(`/mac/${mac}`)
      .then((response)=>{
       this.setState({Mac:response.data})
        console.log(response)
        console.log(this.state.Mac);
      })
  
}
handleLeave = () => {
    this.setState({ showAboutMenu: false });
}

handleHover = ()=> {
  this.setState({ showAboutMenu: true });
}

render()
{
  
const style={
  float:'right'
}
return(
<div>
<div></div>
<div id="wrapper">
  <div id="sidebar-wrapper">
    <ul class="sidebar-nav"  >
    {this.state.Gateways.map((dynamicdata,index) =>
      <li onMouseEnter={this.handleHover.bind(this)} onMouseLeave={this.handleLeave.bind(this)}>
        <a   onClick={this.handleClickthree.bind(this,dynamicdata.Mac)} href="#">{dynamicdata.Name}{":"}{dynamicdata.Mac}</a>
        <div className="submenu-container">
              <CSSTransitionGroup
                transitionName="slide"
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}
              >
                { this.state.showAboutMenu && <Submenu /> }
              </CSSTransitionGroup>
        </div>
    </li>
        )
    }
  </ul>
</div>
<div class="page-content-wrapper">
  <div class="container-fluid">
    <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
      <div class="container">
        <a class="navbar-brand" href="/">Gateway</a>
      <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
      <div class="collapse navbar-collapse" id="navcol-1">
        <ul class="nav navbar-nav mr-auto">
          <li class="nav-item" role="presentation"><a class="nav-link" href="/gateway">Add Gateway</a></li>
          <li class="nav-item" role="presentation"><a class="nav-link" href="/dev">Add Device</a></li>
          <li class="nav-item" role="presentation"><a class="nav-link" href="/mac">Add Settings</a></li>
          <li class="nav-item" role="presentation"><a class="nav-link" href="/con">Connection</a></li>
          <li class="nav-item" role="presentation"><a class="nav-link" href="/devices">Get Devices</a></li>
        </ul>
       
      </div>
    </div>
</nav>
</div>
</div>
</div>
    
    <script src="./js/jquery.min"></script>
    <script src="./js/bootstrap.min"></script>
    <script src="./js/Sidebar-Menu"></script>
<Row>
<Col>
</Col>
<Col>
</Col>
<Col>
{this.state.Form?
   <Forms2 Mac={this.state.Mac}/>
   :null
}      
</Col>
</Row>

</div>
    

   

  );
}

}

export default Sidebars;