



import React, { Component } from 'react';
import {Form, Table,Label} from 'react-bootstrap';
import axios from 'axios';
import { Container } from 'react-grid-system';


class Device extends Component{


  constructor(props){
    super(props);
    this.state={
      Gateways:[],
      Mac:[]
    }
    
  }

componentDidMount(){
      
  axios.get('/gateways')
  .then((response)=>{
    this.setState({
      Gateways:response.data.resp,
    })
    console.log(response.data.resp);
  })
}

handleClickthree(mac){
    this.setState({
      Mac:mac,
    })
    console.log(mac);
    axios.post('/xmpp/:mac',{params:{Mac:mac}})
    .then((response)=>{

      console.log(response)
      console.log(mac);
    })

  } 

  handleClicktwo(id){
    console.log(id);
    axios.delete(`/delete/${id}`)
    .then((response)=>{
      console.log(response);
      console.log(id);
    })
      console.log("wirking");
  } 
handleClick()
{
    var mac = this.refs.mac.value;
    var name = this.refs.name.value;
    var type = this.refs.type.value;

    
    axios.post('/add_dev',{mac:mac,name:name,type:type})
      .then(function(response){
        alert("Connected") 
      console.log(response);
    })
      .catch(function(err){
      console.log(err);
    })
      console.log("working");
    }
    
 



render(){

  console.log(this.props); 
    const style = {
      width:300,
      margin:50
    
    };
    

  return (
    <Container>
    <Table striped bordered condensed hover>
  <thead>
      <th>Mac</th>

  </thead>
  <tbody>
    {
        this.state.Gateways.map((dynamicdata,key) =>
     <tr>
       <td><a href="#" value={dynamicdata.Mac} onClick={this.handleClickthree.bind(this,dynamicdata.Mac)}>{dynamicdata.Mac}</a></td>
    

      
    </tr> 
        )
    
    }    
  </tbody>    

</Table>
    <Form horizontal>
    
      <div class= "form-group">
       <input type="text"  style={style}   class="form-control" value={this.state.Mac} ref="mac"  placeholder="Mac"/>
      </div>
      <div class= "form-group">
        <input type="text"  style={style}  class="form-control" ref="name"   placeholder="Name"/>
      </div>
      <div class= "form-group">
        <input type="text"  style={style} class="form-control" ref="type"   placeholder="Type"/>
      </div>
    
      
      
      <div class= "form-group">
        <input type="button" style={style} class="btn btn-success btn-block" onClick={this.handleClick.bind(this)} value="Add"/>
      </div>
    
</Form>
</Container>

  );
}


}
  
export default Device;
