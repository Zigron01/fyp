
import React , {Component} from 'react';
import {Table} from 'react-bootstrap';
import axios from 'axios';
import {BrowserRouter as Router,Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Forms2 from './Forms2';

class Mtable extends Component {
  constructor(props){
    super(props)
    this.state={
      Xmpp:[],
      Mac:[]
    }
  }
  handleClickone(){
    console.log("get working")
      
    axios.get('/xmpp_settings')
      .then((response)=>{
        this.setState({
          Xmpp:response.data.resp,
        })
        console.log(response.data.resp);
      })
    }

 
    handleClicktwo(id){
      console.log(id);
      axios.delete(`/delete/${id}`)
      .then((response)=>{
        console.log(response);
        console.log(id);
      })
        console.log("wirking");
    } 
  


  handleClickthree(mac){

    console.log(mac);

    axios.get(`/mac/${mac}`)
    .then((response)=>{
     this.setState({Mac:response.data})
      console.log(response)
      console.log(this.state.Mac);
    })

  } 

  render() {
    const style = {
      width:300,
      margin:50
      
  };
  

    return (

  <div>
      <Link to ="/Mac"><input type="button" style={style} class="btn btn-success btn-block" value="Add Settings"/></Link>
      <input type="button" style={style} class="btn btn-success btn-block" value="Get Settings" onClick={this.handleClickone.bind(this)}/>
        
  <Table striped bordered condensed hover>
  <thead>
        <th>Mac</th>
        <th>Port</th>
        <th>Server</th>
        <th>User</th>
        <th>Pass</th>
        <th>Connection</th>
    </thead>
  <tbody>
    {
        this.state.Xmpp.map((dynamicdata,key) =>
     <tr>
       <td><Link to="#" onClick={this.handleClickthree.bind(this,dynamicdata.MacId)}>{dynamicdata.MacId}</Link></td>
        <td>{dynamicdata.Port}</td>
        <td>{dynamicdata.server}</td>
        <td>{dynamicdata.user}</td>
        <td>{dynamicdata.Pass}</td>
        <td><Link to="#" ref="key"  onClick={this.handleClicktwo.bind(this,dynamicdata._id)}>Delete</Link></td>

    </tr> 
        )
    
    }    
  </tbody>    

</Table>
<Forms2 Mac={this.state.Mac}/>
    </div>
    );
  }
}



export default Mtable;