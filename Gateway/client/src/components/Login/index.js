import React ,{Component} from 'react';
import {Form} from 'react-bootstrap'
import "./bootstrap/css/bootstrap.min.css";
import "./fonts/ionicons.min.css";
import "./css/Login-Form-Clean.css";
import "./css/styles.css";
import axios from 'axios';


class Login extends Component
{




handleClick(){
    console.log("working")
    var email = this.refs.email.value;
    var pass = this.refs.password.value;
    axios.post('/log',{email:email,pass:pass})
    .then(function(Response){

        console.log(Response)
    })
    .catch(function(err){
        console.log(err)
    })
    
}

render(){

    return(

<div>
    <div class="login-clean">
        <Form action="/xmpp">
            <h2 class="sr-only">Login Form</h2>

            <div class="illustration">
                <i class="icon ion-ios-navigate"></i>
            </div>
            <div class="form-group">
                <input class="form-control" type="email" ref="email" placeholder="Email"/>
            </div>
            <div class="form-group">
                <input class="form-control" type="password" ref="password" placeholder="Password"/>
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit" onClick={this.handleClick.bind(this)}>Log In</button>
            </div>
            <a href="#" class="forgot">Forgot your email or password?</a>
        </Form>
        </div>
    <script src="./js/jquery.min.js"></script>
    <script src="./bootstrap/js/bootstrap.min.js"></script>
    </div>
    )
    }
}

export default Login;