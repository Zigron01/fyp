
import React , {Component} from 'react';
import {Table} from 'react-bootstrap';
import axios from 'axios';
import {BrowserRouter as Router,Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';


class Tables extends Component {
  constructor(props){
    super(props)
    this.state={
      Gateways:[],
      Mac:''
    }
  }




  handleClick(){
    console.log("working");
  var server = this.refs.server.value;
  var port = this.refs.port.value;
  var jid = this.refs.jid.value;
  var pwd = this.refs.pwd.value;
  var mac = this.refs.mac.value;
  
  
  axios.post('/xmpp',{mac:mac,server:server,port:port,jid:jid,pwd:pwd})
    .then(function(response){
      alert("Connected") 
    console.log(response);
  })
    .catch(function(err){
    console.log(err);
  })
    console.log("working");
  }
  
  componentDidMount(){
    console.log("get working")
      
    axios.get('/gateways')
      .then((response)=>{
        this.setState({
          Gateways:response.data.resp,
        })
        console.log(response.data.resp);
      })
    }

  handleClicktwo(id){
    console.log(id);
    axios.delete(`/delete/${id}`)
    .then((response)=>{
      console.log(response);
      console.log(id);
    })
      console.log("wirking");
  } 


  handleClickthree(mac){
    this.setState({
      Mac:mac,
    })
    console.log(mac);
    axios.post('/xmpp/:mac',{params:{Mac:mac}})
    .then((response)=>{

      console.log(response)
      console.log(mac);
    })

  } 
  render() {
    const style = {
      width:300,
      margin:50
      
  };
  

    return (


  <div>
     

          
  <Table striped bordered condensed hover>
  <thead>
      <th>Mac</th>
      <th>Name</th>
      <th>Email</th>
      <th>Delete  </th>
  </thead>
  <tbody>
    {
        this.state.Gateways.map((dynamicdata,key) =>
     <tr>
       <td><a href="#" value={dynamicdata.Mac} onClick={this.handleClickthree.bind(this,dynamicdata.Mac)}>{dynamicdata.Mac}</a></td>
        <td>{dynamicdata.Name}</td>
        <td>{dynamicdata.Email}</td>
        <td><a href="#" ref="key"   onClick={this.handleClicktwo.bind(this,dynamicdata._id)}>Delete</a></td>

    </tr> 
        )
    
    }    
  </tbody>    

</Table>

    <div class= "form-group">
        <input type="text"  style={style} class="form-control" value={this.state.Mac}   ref="mac"  placeholder="Mac"/>
    </div>
    <div class= "form-group">
        <input type="text"  style={style} class="form-control" ref="server"  placeholder="Server"/>
      </div>
      <div class= "form-group">
        <input type="text"  style={style} class="form-control" ref="port"  placeholder="Port"/>
      </div>
      <div class= "form-group">
        <input type="text"  style={style} class="form-control" ref="jid"  placeholder="user"/>
      </div>
      <div class= "form-group">
        <input type="text"  style={style} class="form-control" ref="pwd"  placeholder="password"/>
      </div>
      <div class= "form-group">
        <input type="button" style={style} class="btn btn-success btn-block" onClick={this.handleClick.bind(this)} value="submit"/>
      </div> 
    </div>
    );
  }
}



export default Tables;