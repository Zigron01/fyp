

import React, { Component } from 'react';
import {Form, Table,Label} from 'react-bootstrap';
import axios from 'axios';
import { Container } from 'react-grid-system';


class GetDevices extends Component{


  constructor(props){
    super(props);
    this.state={
      Gateways:[],
      Mac:[]
    }
    
  }

componentDidMount(){
      
  axios.get('/gateways')
  .then((response)=>{
    this.setState({
      Gateways:response.data.resp,
    })
    console.log(response.data.resp);
  })
}

handleClickthree(mac){
    this.setState({
      Mac:mac,
    })
    console.log(mac);
    axios.post('/xmpp/:mac',{params:{Mac:mac}})
    .then((response)=>{

      console.log(response)
      console.log(mac);
    })

  } 

  handleClicktwo(id){
    console.log(id);
    axios.delete(`/delete/${id}`)
    .then((response)=>{
      console.log(response);
      console.log(id);
    })
      console.log("wirking");
  } 
handleClick()
{
    var mac = this.refs.mac.value;
    var name = this.refs.name.value;
    var type = this.refs.type.value;
    var xml = this.refs.Cxml.value
    
    axios.post('/get_dev',{mac:mac,name:name,type:type,xml:xml})
      .then(function(response){
        alert("Connected") 
      console.log(response);
    })
      .catch(function(err){
      console.log(err);
    })
      console.log("working");
    }
    
    handleChangeSet(event){
        this.setState({cmds:  this.refs.selector.value });
        console.log(this.refs.selector.value);
        this.refs.xml.value = '<?xml version="1.0" encoding="UTF-8"?><p><mac v="00:1D:94:01:03:61"/><cmds><cmd a='+this.refs.selector.value+'/></cmds></p>';
      
      
      }    
 



render(){

  console.log(this.props); 
    const style = {
      width:300,
      margin:50
    
    };
    

  return (
    <Container>
  
    <Form horizontal>
      <div class= "form-group">
        <input type="text"  style={style}  class="form-control" ref="name"   placeholder="Name"/>
      </div>
      <div class= "form-group">
        <input type="text"  style={style} class="form-control" ref="type"   placeholder="Type"/>
      </div>
      <div class="form-group" style={style} >
        <label for="exampleFormControlSelect1"><b>Xml Commands</b></label>
        <select  ref="selector"  class="form-control" onChange ={this.handleChangeSet.bind(this)}>
          <option value="">Select</option>
          <option value='"getDeviceStatus"'>GetDeviceStatus</option>
        <option value='"getNet"'>GetNet</option>
        <option value='"getDateTime"'>getDateTime</option>
        <option value='"getPanel"'>getPanel</option>
        <option value='"getSystem"'>getSystem</option>
    </select>
      </div> 
      <div class="form-group" style={style}>
  <label for="exampleFormControlTextarea1">Example textarea</label>
    <textarea class="form-control" ref="xml" rows="">
    
    </textarea>
    </div>
      
      <div class= "form-group">
        <input type="button" style={style} class="btn btn-success btn-block" onClick={this.handleClick.bind(this)} value="Get Device"/>
      </div>
    
</Form>
</Container>

  );
}


}
  
export default GetDevices;
