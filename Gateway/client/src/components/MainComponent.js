import React, { Component } from 'react';
import Home from './HomeComponent';
import Sidebars from './Sidebar';
import { Container, Row, Col } from 'react-grid-system' ;
import Header from './HeaderComponent';
import {Switch,Route,Redirect} from 'react-router-dom';
import {BrowserRouter} from 'react-router-dom';
import Forms2 from './Forms2';
import Tables from './Tables';
import Tables2 from './Tables2';
import Mtable from './Mtable';
import HomeComponent from './HomeComponent';
import HeaderComponent from './HeaderComponent';
import { NavLink } from 'react-router-dom';
import Login from './Login/index';
import Connection from './connection';
import Device from './Devices';
import GetDevices from './GetDevices'



class Main extends Component{
render(){


  
return (
<div className="Main">

    
    <Route path ="/" exact strict render={
    ()=>{
      return(
        <div>
    
        
       <Login/>
       
      </div>
        
      
       )
    }
  }>
</Route>
<Route path ="/devices" exact strict render={
    ()=>{
      return(
        <div>

      <Row>
        <Sidebars/>
        <GetDevices/>
      </Row> 
      </div>
        
      
       )
    }
  }>
</Route>
<Route path ="/dev" exact strict render={
    ()=>{
      return(
        <div>

      <Row>
        <Sidebars/>
        <Device/>
      </Row> 
      </div>
        
      
       )
    }
  }>
</Route>
<Route path ="/xmpp" exact strict render={
    ()=>{
      return(
        <div>
      <Row>
        <Sidebars/>
      </Row> 
      </div>
        
      
       )
    }
  }>
</Route>
<Route path="/con" exact strict render={
  ()=>{
    return(
      <div>
        <Row>
       <Sidebars/>
       <Connection/>
       </Row> 
</div>
    )
   }
 }>


</Route>
<Route path="/Mac" exact strict render={
  ()=>{
    return(
      <div>
        <Row>
      
        <Sidebars/>
      
         <Tables2/>
      </Row>
      </div>
    )
  }
}>
</Route>
<Route path="/gateway" exact strict render={
  ()=>{
    return(
      <div>
    <Row>
    <Sidebars/>
    <HomeComponent/>
    </Row>
      </div>
    )
  }
}>
</Route>

</div>



  );
}


}
  
export default Main;