import React, { Component } from 'react';
import {
  Navbar,Nav,NavbarBrand,NavItem, 
  DropdownToggle,UncontrolledDropdown,
  DropdownItem,DropdownMenu,Collapse,
  NavbarToggler,NavLink}
  from 'reactstrap';

//import {NavLink} from 'react-router-dom';



class Header extends Component{
    constructor(props){
        super(props);
 
        
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">Gateway</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav tabs>
              <NavItem>
                <NavLink href="/" active>Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/Mac" activeStyle={{color:'green'}}>Mac Setting</NavLink>
              </NavItem>
              <NavItem>
                <NavLink  href="/xmpp" activeStyle={{color:'green'}}>Xmpp Setting</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/con" activeStyle={{color:'green'}}>Connection</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Options
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Option 1
                  </DropdownItem>
                  <DropdownItem>
                    Option 2
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    Reset
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }

}


export default Header;