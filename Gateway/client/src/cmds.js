var setcmds = {
    setSystem : {
        ac_t: '',
        jam_t:''
    },
    setWirelessNet : {
        lanType:'',
        ip:'',
        mask:'',
        gw:'',
        dns1:'',
        dns2:''
    },
    setNTP:{
        ntpsync:'',
        ntpsvr:''
    },
    setDateTime:{
        date :'',
        time :'',
        timezone :'',
        ntpsync:'',
        ntpsvr:''
    },
    setTZ:{
      timezone:''  
    },
    setVoiceRpt:{
        tel:'',
        tel_srv:'',
        folon:'',
        cb_timer:''
    }

}
module.exports = {data:setcmds}