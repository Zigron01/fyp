import React, { Component } from 'react';
import logo from './logo.svg';

import Header from './components/HeaderComponent';
import {Switch,Route,Router,Redirect} from 'react-router-dom';
import {BrowserRouter} from 'react-router-dom';
import Forms2 from './components/Forms2';
import Tables from './components/Tables';
import Tables2 from './components/Tables2';
import Mtable from './components/Mtable';
import './App.css';
import Main from './components/MainComponent'

class App extends Component{
render(){

return(
 
<BrowserRouter>

 <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title" >Welcome to React</h1>
 
        </header>
  
  <Main/>

   

</div>
</BrowserRouter>

  );
}


}
  
export default App;