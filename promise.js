//one promise
var somePromise = new Promise((resolve,reject)=>{
    resolve('Hello It worked');
    reject("Unable to fullfilled promise");
});



//other promise
var asyncAdd = (a,b) => {
    return new Promise((resolve,reject) => {
        
      if(typeof a === 'number' && typeof b === 'number'){  
        resolve(a+b);
      }else{
          reject("Arguments must be a number ");
      }
    
    
    });
};

asyncAdd(2,3).then((res)=>{
    console.log("Result : " ,res);
    return asyncAdd(res,22);
},
(errorMessage)=>{
    console.log("Error : ",errorMessage);}).
    then((res)=>{
        console.log("Result : " ,res);
    })
    return somePromise.then((message)=>{
        console.log("Success :" , message);
        }).catch((errorMessage)=>{
            console.log("Error :",errorMessage);
    })








