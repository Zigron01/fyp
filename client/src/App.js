import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Form} from 'react-bootstrap';
import axios from 'axios';
import setcmds from './cmds.js';

class App extends Component {
  constructor(props){
    super(props);
 
    this.state={
      Gresponse:''
    },
    this.state={
      Cresponse:''
    }
    this.state = {
      xml: '<?xml version="1.0" encoding="UTF-8"?><p><mac v="00:1D:94:01:08:16"/><cmds><cmd a='+this.state.cmds+'/></cmds></p>'
    }
  
  
    this.handleClick = this.handleClick.bind(this);
    
}



handleClick(){
  //var Gurl = this.refs.Gurl.value;
  var Gurl = this.refs.Gurl.value;
  var Curl = this.refs.Curl.value;
  var username = this.refs.username.value;
  var password = this.refs.password.value;
  var Gxml = this.refs.Gxml.value;
  var Cxml = this.refs.Cxml.value;  
  
  var self = this;


  if(this.refs.Gurl.value){  
  axios.post('/panel', {username: username, password: password, xml:Gxml, url: Gurl})
      .then( (resp) => {
        self.refs.Gresponse.value = resp.data.response; 
      })
      .catch(err=>console.log(err));
    }
  if(this.refs.Curl.value){
    axios.post('/panel', {username: username, password: password, xml:Cxml, url: Curl})
      .then( (resp) => {
        self.refs.Cresponse.value = resp.data.response; 
      })
      .catch(err=>console.log(err));
    }
}
storedValueHandler(id){
  console.log(id)
  this.setState({cmds:id})  
}


handleChange(event){
  this.setState({cmds:  this.refs.cmdselector.value });
  console.log(this.refs.cmdselector.value);
  this.refs.Gxml.value = '<?xml version="1.0" encoding="UTF-8"?><p><mac v="00:1D:94:01:08:16"/><cmds><cmd a='+this.refs.cmdselector.value+'/></cmds></p>';
}
handleChangeSet(event){
  this.setState({cmds:  this.refs.selector.value });
  console.log(this.refs.selector.value);
  this.refs.Cxml.value = '<?xml version="1.0" encoding="UTF-8"?><p><mac v="00:1D:94:01:08:17"/><cmds><cmd a='+this.refs.selector.value+'/></cmds></p>';


}
 
/*var cmd = '<?xml version="1.0" encoding="UTF-8"?><p><mac v="00:1D:94:01:08:16"/><cmds><cmd a='+this.refs.selector.value+'/></cmds>'  
this.refs.xml.value= cmd+'<date v ='+this.refs.date.value+'/><time v='+this.refs.time.value+'/><timezone v='+this.refs.timezone.value+'/><ntpsync v='+this.refs.ntpsync.value+'/><ntpsvr v='+this.refs.ntpsvr.value+'/></p>'
var data = setcmds.data.setSystem;
var val = <Form key1={data.date} key2={data.time} key3={data.timezone} key4={data.ntpsync}
            key5={data.ntpsvr}/>; 
  console.log(val);  
  } 
} */


render() {
const select = {
  paddingleft:100,
    margin:30,
  width:500
}
const style = {
    width:1000,
    paddingleft:100,
    margin:20
};
const style1={
  width:100,
  paddingleft:100,
  margin:20
}
const button ={
  width:1000,
  paddingleft:100,
  margin:20
}
return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title" >Welcome to React</h1>
        </header>
        <h1>Response from express</h1>{this.state.response}
        
        <Form horizontal>
          <div class="form-group" style={style}>
            <input type="url" class="form-control" ref="Gurl"  placeholder="G-URL"/>
        </div>

          <div class="form-group" style={style}>
            <input type="url" class="form-control" ref="Curl"  placeholder="C-URL"/>
        </div>
        <div class="form-group" style={style}>
          <input type="text" class="form-control" ref="username" placeholder="Username" value={this.state.username}/>
        </div>
        <div class="form-group" style={style}>
          <input type="password" class="form-control" ref="password" placeholder="Password"/>
        </div>

    <div class="row">
    <div class="col-offset-md-3">
      <div class="form-group" style={select}>
        <label for="exampleFormControlSelect1"><b>Camera Commands</b></label>
        <select  ref="selector"  class="form-control" onChange ={this.handleChangeSet.bind(this)}>
          <option value="">Select</option>
          <option value='"getDev"'>GetDev</option>
        <option value='"getNet"'>GetNet</option>
        <option value='"getDateTime"'>getDateTime</option>
        <option value='"getPanel"'>getPanel</option>
        <option value='"getSystem"'>getSystem</option>
    </select>
      </div>
     </div>
     <div class="col-md-3" > 
      <div class="form-group" style={select}>
          <label for="exampleFormControlSelect1"><b>Gateway Commands</b></label>
      <select  ref="cmdselector" class="form-control" onChange = {this.handleChange.bind(this)}>
        <option>Select</option>
        <option value='"getDev"'>GetDev</option>
        <option value='"getNet"'>GetNet</option>
        <option value='"getDateTime"'>getDateTime</option>
        <option value='"getPanel"'>getPanel</option>
        <option value='"getSystem"'>getSystem</option>
        <option value='"getGSM"'>getGSM</option>
        <option value='"getRpt"'>getRpt</option>
        <option value='"getVoiceRpt"'>getVoiceRpt</option>
        <option value='"getHA"'>getHA</option>
        <option value='"getAMR"'>getAMR</option>
        <option value='"getThermostat"'>getThermostat</option>
        <option value='"getPowerMeter"'>getPowerMeter</option>
        <option value='"getDeviceGroup"'>getDeviceGroup</option>
        <option value='"getPoll"'>getPoll</option>
        <option value='"getXMPP"'>getXMPP</option>
        <option value='"getSMTP"'>getSMTP</option>
        <option value='"getTestIP"'>getTestIP</option>
        <option value='"getDDNS"'>getDDNS</option>
        <option value='"getUPnP"'>getUPnp</option>
        <option value="getNote">getNote</option>
        <option value='"getHistory"'>getHistory</option>
    </select>
</div>

{this.state.name}
</div>
<div class="form-group" style={style}>
  <label for="exampleFormControlTextarea1">Example textarea</label>
    <textarea class="form-control" ref="Gxml" rows="3">
    
    </textarea>
  </div>
  <div class="form-group" style={style}>
  <label for="exampleFormControlTextarea1">Example textarea</label>
    <textarea class="form-control" ref="Cxml" rows="3">
      
    </textarea>
  </div>
  <input type="button" style={style} class="btn btn-success btn-block" onClick={this.handleClick} value="send" />
<div class="row">
<div class="col-offset-md-5">
  <div class ="form-group" style={select}>
    <label for ="exampleFormControlTextarea1"><b>Gateway Response</b></label>
      <textarea ref="Gresponse" rows="10" class="form-control">
        {this.state.Gresponse}
      </textarea>
  </div>
</div>
<div class="col-sm-5">
<div class ="form-group" style={select}>
    <label for ="exampleFormControlTextarea1"><b>Camera Response</b></label>
      <textarea ref="Cresponse" rows="10" class="form-control">
        {this.state.Cresponse}
      </textarea>
  </div>
</div>
</div>  
</div>
  </Form> 
</div>
  
    );
  }
}
export default App;
