var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var xmlcompare = require('node-xml-compare');
var async = require('async');
var rp = require('request-promise');
var fetch = require('node-fetch');

//configure the body parse
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/test',function(req,res) {
   res.status(200).json(req.body);
});

app.post('/panel', function (req, res) {

  console.log(req.body.Cxml)
  var URL = req.body.url;
  var auth = "Basic " + new Buffer(req.body.username + ":" + req.body.password).toString("base64");

  const options = {
    method: 'POST',
    url: URL,
    headers: {
      'Authorization': auth,
      Accept: 'text/xml',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    formData: {
      strXML: req.body.xml
    }

  };


  console.log(options);
  // rp(options)
  //   .then(response => response.json({response}))
  //   .then(json => console.log(json))
  //   .catch(err => console.log(err));


  // });
  rp(options).then(function(response){
    res.status(200).json({response:response});
  }).catch(function(error){
    res.status(500).json({message: error});
  });
});




app.listen(1000, function (req, res) {
  console.log("runnig on 1000");
});